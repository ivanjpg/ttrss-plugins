# ttrss-plugins

Currently there's only one plugin, **showexplosm** that extracts the comic from the [Explosm](http://explosm.net) comic feed and shows the image inside [TT-RSS](https://tt-rss.org/).

There's almost no documentation about writting plugins to TT-RSS. This work is partially done heuristically based on the other plugins present by default an TT-RSS.

## Install
- Place the plugin folder inside `plugins.local` at your *TT-RSS* installation dir.
- Go to `Preferences -> Plugins` menu entry and activate the plugin.
