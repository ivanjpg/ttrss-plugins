/* This plugin takes the Explosm.net feed and
 * extracts the comic image to be shown
 * at the TT-RSS reader as the only content.
 */

<?php
class ShowExplosm extends Plugin {
	private $host;

	function about() {
		return
		array(
			0.1,										// Plugin version
			"Plugin to show Explosm.net comic image",	// Description
			"IvanJPG",									// Author
			false										// Is this a system plugin?
		); // array()
	} // about()

	function init($host) {
		$this->host = $host;

		$host->add_hook($host::HOOK_ARTICLE_FILTER, $this);
	} // init()

	function hook_article_filter($article) {
		if( strpos($article['link'], 'http://explosm.net/comics/') === FALSE ) {
			return $article;
		}

		$doc = new DOMDocument();
		@$doc->loadHTML( fetch_file_contents($article['link']) );

		if(!$doc) {
			return $article;
		}

		$comic = $doc->getElementById('main-comic');

		if($comic == NULL) {
			return $article;
		}

		$src = $comic->getAttribute('src');

		if( empty($src) ) {
			return $article;
		}

		$article['content'] = "<img src=\"{$src}\">";

		return $article;
	} // hook_render_article()

	function api_version() {
		return 2;
	} // api_version()
} // ShowExplosm